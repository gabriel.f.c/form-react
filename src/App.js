import UserForm from './components/UserForm/index';
import Home from './pages/home';
import './App.css';
import { useState } from 'react';

function App() {

  const [msg, setMsg] = useState({ name: "", telefone: "", email: "" })

  return (
    <div className="App">
      <main className="App-header">
        <UserForm setMsg={setMsg} />
        <Home data={msg} />
      </main>
    </div>
  );
}

export default App;