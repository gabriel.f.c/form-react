import "./style.css"

function Home({ data }) {
    return (
        <div id="screen" className="homescreen">
            <div id="card" className="homescreen__wrapper">
                <h1>Bem-vindo, {data.name}</h1>
                <div className="homescreen__contact">
                    <div>
                        <h3>Contato</h3>
                        <h3>{data.telefone}</h3>
                    </div>
                    <div>
                        <h3>E-mail</h3>
                        <h3>{data.email}</h3>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Home