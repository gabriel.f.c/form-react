import './style.css'
import * as yup from 'yup';
import { useForm } from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';

function UserForm({ setMsg }) {

    const schema = yup.object().shape({
        name: yup.string().min(4, "Mínimo de 4 caractéres.").max(18, "Máximo de 18 caractéres.").required("Nome obrigatório"),
        email: yup.string().email("Deve ser um e-mail válido.").required("E-mail obrigatório."),
        telefone: yup.string().min(14, "Deve conter DDD.").required("Telefone obrigatório"),
        password: yup.string().min(8, "Senha deve conter 8 caractéres."),
        confirm: yup.string().oneOf([yup.ref("password")], "Senhas diferentes")
    });

    const { register, handleSubmit, formState: { errors } } = useForm({ resolver: yupResolver(schema), })

    const handleWelcome = (data) => {
        setMsg(data)
        const box = document.getElementById('login')
        const screen = document.getElementById('screen')
        const card = document.getElementById('card')
        box.style.display = "none"
        card.style.display = "flex"
        screen.style.filter = "blur(0px)"
    }

    return (
        <>
            <form onSubmit={handleSubmit(handleWelcome)} id="login" className="container">
                <div className="container__input">
                    <div className="subcontainer__passlabel">
                        <label>Usuário</label>
                        <p>{errors.name?.message}</p>
                    </div>
                    <input {...register("name")} className="container__field" placeholder="Nome" />
                </div>
                <div className="container__input">
                    <div className="subcontainer__passlabel">
                        <label>E-mail</label>
                        <p>{errors.email?.message}</p>
                    </div>
                    <input {...register("email")} className="container__field" placeholder="E-mail" />
                </div>
                <div className="container__input">
                    <div className="subcontainer__passlabel">
                        <label>Telefone</label>
                        <p>{errors.telefone?.message}</p>
                    </div>
                    <input {...register("telefone")} className="container__field" placeholder="Telefone - Ex. (21) 9 9999-9999" />
                </div>
                <div className="container__input">
                    <div className="subcontainer__passlabel">
                        <label>Senha</label>
                        <p>{errors.password?.message}</p>
                    </div>
                    <div className="subcontainer__passinput">
                        <input {...register("password")} className="container__field sub" placeholder="Senha" />
                        <input {...register("confirm")} className="container__field sub" placeholder="Confirmar Senha*" />
                    </div>
                </div>
                <div className="container__input">
                </div>
                <div className="container__input center">
                    <button className="container__button" type="submit">Cadastrar</button>
                </div>
            </form>
        </>
    )

}

export default UserForm